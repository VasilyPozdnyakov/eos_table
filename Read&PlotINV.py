# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 16:54:17 2019

@author: Pozdnyak
"""
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk
from tkinter import filedialog
import sys
import os


def read_numbers(file,test,n_elements,var):
    for x in range (0,n_elements):
        if var==4:
            file.read(1)
            var=0
        var=var+1
        test.append(float(file.read(15)))
    return var

def graph_plot (numb, x, y, xlabel, ylabel, title):
    plt.subplot(numb)
    plt.plot (x, y, 'ro', linewidth=2, markersize=2)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.grid(True)


######################## open file #################################################
root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename(
    filetypes=[('.INV and .301 files', '.301 .INV',)], title='Open file')
file_name = os.path.split(file_path)[1]


######################### Read INV #########################################
if file_path!='':
    f = open(file_path, "r")
else:
    tk.messagebox.showinfo(title='Warning',
                               message='Run again and open a file .INV or .301')
    sys.exit ()
    
info=[]
k=read_numbers (f, info, 4, 0)  # Read info
   
if (file_path.endswith('.INV')):
    ndens = int(info[2])
    dens = []
    k=read_numbers (f, dens, ndens, k)  #Read densities INV
    ne = int(info[3])
    e = []
    k=read_numbers (f, e, ne, k)  # Read energies INV
    e0 = []
    k=read_numbers (f, e0, ndens, k)  # Read e0 (T=0) INV

################# read pressures INV ############################
    P = [[0] * ndens for i in range(ne)]
    for x in range (0, ne):
        for y in range (0, ndens):
            if k==4:
                f.read(1)
                k=0
            k=k+1
            P[x][y]=float(f.read(15))/100.0

############## read tempreatures INV #############################
    T = [[0] * ndens for i in range(ne)]
    for x in range (0, ne):
        for y in range (0, ndens):
            if k==4:
                f.read(1)
                k=0
            k=k+1
            T[x][y]=float(f.read(15))/100.0
            
else:
    if (file_path.endswith('.301')):
        info_301=[]
        
        ndens_301 = int(info_301[2])
        dens_301 = []
        k=read_numbers (f, dens_301, ndens_301, k)  # Read densities INV
        
        nT_301 = int(info_301[3])
        T_301=[]
        k=read_numbers (f, T_301, nT_301, k)  # Read temperatures T reference
    else:
        tk.messagebox.showinfo(title='Warning',
                               message='Run again and open a file .INV or .301')
        sys.exit ()
    
f.close()


########!!!!!!!!! Read T reference from 301 !!!!!!!!!!!################
f_ref = open("H2OTest2020.301", "r")

info_ref=[]
k=0
k=read_numbers(f_ref, info_ref, 4, k) #read info1

ndens_ref = int(info_ref[2])
dens_ref = []
k=read_numbers (f_ref, dens_ref, ndens_ref, k)  # Read densities INV

nT_ref = int(info_ref[3])
T_ref=[]
k=read_numbers (f_ref, T_ref, nT_ref, k)  # Read temperatures T reference

f_ref.close()


############### Choose density point #####################
#numb = simpledialog.askinteger(title="Number", prompt="Enter density point (number)")
numb=110
print ('Density in point #'+str(numb)+' is '+str(dens[numb]))


################ Interpolation #####################
xP=[0]*ne
xT=[0]*ne
#xD=[0]*ndens  

for j in range (0,ne):
    xT[j]=T[j][numb]

for j in range (0,ne):
    xP[j]=P[j][numb]
    
Eint=np.interp(T_ref,xT,e)
#for x in range (0, ndens):
Pint=np.interp(Eint,e,xP)
  


############################ Plot #######################################

fig=plt.figure(figsize=(10,8))
fig.suptitle(file_name)

####### plot e0
graph_plot (221, dens, e0, 'Density [g/cm3]', 'E0 [Mbar*cm3/g]', 'Cold curve (e0)')
#plt.xlim(0,70)
#plt.ylim(0,0.3)

####### plot e(T) at dens_i
graph_plot (222, xT, e, 'T [K]', 'E [MJ/kg]', 'E (T), dens='+str(dens[numb]))
#plt.xlim(0,7.5e6)
#plt.ylim(0,800)

####### plot pressure P(e) at dens_i
graph_plot (223, e, xP, 'E [MJ/kg]', 'Pressure [GPa]', 'P (E), dens='+str(dens[numb]))

####### plot pressure P(T) at dens_i
graph_plot (224, T_ref, Pint, 'T [K]', 'Pressure [GPa]', 'P (T), dens='+str(dens[numb]))


'''
i=90
print ('Energy in point #'+str(i)+' is '+str(e[i]))
for j in range (0,ndens):
    xD[j]=P[i][j]
  
#plot pressure P(dens) at e_i
graph_plot (224, dens, xD, 'Density [g/cm3]', 'Pressure [GPa]', 'P (dens), e='+str(e[i]))

'''


plt.tight_layout()
plt.show()


