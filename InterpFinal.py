# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 16:54:17 2019

@author: Pozdnyak
"""
import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal

info=[]
dens=[]
T=[]
e0=[]
eour=[]
Tint=[]
Pint=[]
k=0

f = open("H2OTest2020.301", "r")
#Read info
for x in range (0,4):
    info.append(float(f.read(15)))
f.read(1)

print (info)
ndens = int(info[2])
nT = int(info[3])

P = [[0] * ndens for i in range(nT)]
e = [[0] * ndens for i in range(nT)]

Pinv = [[0] * nT for i in range(ndens)]
einv = [[0] * nT for i in range(ndens)]

Pintinv = [[0] * ndens for i in range(nT)]
Tintinv = [[0] * ndens for i in range(nT)]


#read densities
for x in range (0, ndens):
    if k==4:
        f.read(1)
        k=0
    k=k+1
    dens.append(float(f.read(15)))

#read temperatures
for x in range (0, nT):
    if k==4:
        f.read(1)
        k=0
    k=k+1
    T.append(float(f.read(15)))

#read pressures
for x in range (0, nT):
    for y in range (0, ndens):
        if k==4:
            f.read(1)
            k=0
        k=k+1
        P[x][y]=float(f.read(15))/100.0

#read energies
for x in range (0, nT):
    for y in range (0, ndens):
        if k==4:
            f.read(1)
            k=0
        k=k+1
        e[x][y]=float(f.read(15))/100.0

#read e0 (T=0)
for x in range (0, ndens):
    if k==4:
        f.read(1)
        k=0
    k=k+1
    e0.append(float(f.read(15))/100.0)    
f.close()


#plot graph
plt.plot (dens, e0, linewidth=2)
plt.xlabel('density [g/cm3]', fontsize=16)
plt.ylabel('energy0 [Mbar*cm3/g]', fontsize=16)
plt.title('Interpolation FEOS, cold curve (e0)', fontsize=18)
plt.grid(True)
plt.xlim([0,80])
plt.ylim([0.0,40])
plt.show()

#what is e?=e0+de?
'''for i in range (0,ndens):
    print (e[30][i]-e0[i])'''

#Change density and temperatures
for x in range (0, nT):
    for y in range (0, ndens):
        Pinv[y][x]=P[x][y]
        einv[y][x]=e[x][y]


#read energies we will use
f = open("AU_initM2D.txt", "r")
#skip info
for x in range (0,4):
    f.read(15)
f.read(1)
#skip densities
for x in range (0, ndens):
    if k==4:
        f.read(1)
        k=0
    k=k+1
    f.read(15)
#read energies
for x in range (0, nT):
    if k==4:
        f.read(1)
        k=0
    k=k+1
    eour.append(float(f.read(15)))
f.close()


#interpolation of temperature
for x in range (0, ndens):
    Tint.append(np.interp(eour,einv[x],T))

#interpolation of pressure
for x in range (0, ndens):
    Pint.append(np.interp(Tint[x],T,Pinv[x]))



#plot interpolated pressure
plt.figure(figsize=(9,7))
plt.plot (eour, Pint[32], 'ro', linewidth=2, label='Pint')
plt.plot (eour, Pint[31], 'k*', linewidth=2, label='Pint')
plt.xlabel('Energy [Mbar cm3/g]', fontsize=16)
plt.ylabel('P [Mbar]', fontsize=16)
plt.title('Interpolation FEOS, P', fontsize=18)
#plt.xlim([0,600])
#plt.ylim([0.0,20])
plt.grid(True)


'''
plt.figure(figsize=(9,7))
plt.plot (eour, Tint[32], linewidth=2, label='Tint')
plt.grid(True)
plt.legend()
plt.xlim([0,0.5])
plt.ylim([0.0,1e5])'''
'''
#plot interpolated temperature
plt.figure(figsize=(9,7))
plt.plot (eour, Tint[50], linewidth=2, label='Tint')
plt.xlabel('Energy [Mbar cm3/g]', fontsize=16)
plt.ylabel('Temperatre [K]', fontsize=16)
plt.title('Interpolation FEOS, T and Tinv comparison', fontsize=18)
plt.xlim([0,0.5])
plt.ylim([0.0,1e5])

#initial data
plt.plot (einv[50], T, linewidth=2, label='T')
plt.legend()
plt.grid(True)'''
plt.show()




#Change density and temperatures
for x in range (0, nT):
    for y in range (0, ndens):
        Pintinv[x][y]=Pint[y][x]
        Tintinv[x][y]=Tint[y][x]


#write in a file
f = open("TEST2020.INV", "w")
k=0

#write info
for i in range (0,4):
    a=info[i]
    if a>=0:
        f.write(' ')
        f.write('%.8e' % Decimal(a))
    else:
        f.write('%.8e' % Decimal(a))
    k=k+1
    if k==4:
        f.write('\n')
        k=0

#write densities
for i in range (0,ndens):
    a=dens[i]
    if a>=0:
        f.write(' ')
        f.write('%.8e' % Decimal(a))
    else:
        f.write('%.8e' % Decimal(a))
    k=k+1
    if k==4:
        f.write('\n')
        k=0
        
#write energies
for i in range (0,nT):
    a=eour[i]
    if a>=0:
        f.write(' ')
        f.write('%.8e' % Decimal(a))
    else:
        f.write('%.8e' % Decimal(a))
    k=k+1
    if k==4:
        f.write('\n')
        k=0
        
#write e0
for i in range (0,ndens):
    a=e0[i]
    if a>=0:
        f.write(' ')
        f.write('%.8e' % Decimal(a))
    else:
        f.write('%.8e' % Decimal(a))
    k=k+1
    if k==4:
        f.write('\n')
        k=0  
        
#write pressure
for x in range (0, nT):
    for y in range (0, ndens):
        a=Pintinv[x][y]
        if a>=0:
            f.write(' ')
            f.write('%.8e' % Decimal(a))
        else:
            f.write('%.8e' % Decimal(a))
        k=k+1
        if k==4:
            f.write('\n')
            k=0
        
#write temperatures
for x in range (0, nT):
    for y in range (0, ndens):
        a=Tintinv[x][y]
        if a>=0:
            f.write(' ')
            f.write('%.8e' % Decimal(a))
        else:
            f.write('%.8e' % Decimal(a))
        k=k+1
        if k==4:
            f.write('\n')
            k=0
      

f.write(' ')
f.write('%.8e' % Decimal(0.0))
f.write(' ')
f.write('%.8e' % Decimal(0.0))     
f.close()





